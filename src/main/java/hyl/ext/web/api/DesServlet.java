package hyl.ext.web.api;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import hyl.core.AIni;
import hyl.core.MyFun;
import hyl.core.safe.MyAes;

@WebServlet(name = "jiami", urlPatterns = "/ex/hyl_api_jiami")
// 扩展 HttpServlet 类
public class DesServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void init() throws ServletException {

	}

	

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 设置响应内容类型
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String 待加密字符串 = request.getParameter("s");
		//System.out.println(待加密字符串);
		if (MyFun.isBlank(待加密字符串)) {
			out.println("");
			return;
		}
		/**
		 * p 是系统间的约定编号, 通过变换p实现不同的加密 
		 */
		String 密钥后缀 = MyFun.nvlStr(request.getParameter("p"), "");
		out.println( MyAes.f加密64(待加密字符串,AIni.PKEY+密钥后缀));	
	}

	public void destroy() {
		// 什么也不做
	}
}