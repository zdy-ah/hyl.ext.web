package hyl.ext.web.ms;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Map;
import hyl.base.cache2.ExToken;
import hyl.base.cache2.ExTokenFactory;
import hyl.core.Amy;
import hyl.core.MyFun;
import hyl.core.conf.MyConfig;
import hyl.core.data.ExBlockMap;
import hyl.core.db.MyDB;
import hyl.core.net.pg.Netpg2Msgs1;
import hyl.core.run.IDo1;
import hyl.core.run.IReply;
import hyl.core.safe.MyRsa;

public class TMs {
	/**
	 * 核心内含属性
	 */
	public static final int I未知 = 0;
	public static final int I注册 = 1;
	public static final int IPING = 2;
	public static final int IERR = 3;

	public static final int I内部请求 = 10;
	public static final int I内部应答 = 11;
	public static final int I收到 = 12;
	public static final int I请求验证令牌 = 20;
	public static final int I应答令牌验证 = 21;

	public static final int I请求处理 = 1001;
	public static final int I请求应答 = 1002;
	public static final String S验证令牌 = "验证令牌";
	public static final String S获取令牌 = "获取令牌";
	public static final String S验证身份 = "验证身份";
	public static final String S获取会话 = "获取会话";

	public BInfo _binfo;
	public transient PublicKey K公钥 = null;
	public transient PrivateKey K私钥 = null;

	// 接收消息原型
	protected transient Netpg2Msgs1 _npg = new Netpg2Msgs1();
	public IDo1<Netpg2Msgs1> on收到消息处理 = null;
	/**
	 * return 的 Netpg2Msgs1 对象 的 cmd 固定为 I请求应答 1002
	 */
	public IReply<Netpg2Msgs1, Netpg2Msgs1> on收到消息处理并反馈 = null;
	/**
	 * 不能去除 测试时 需要 指定不同的库 默认是db1
	 */
	public String _库别名 = Amy.DB1;

	public void setDB(String 库别名) {
		_库别名 = 库别名;
	}

	public MyDB getDB() {
		return MyDB.createDB(_库别名);
	}

	// static final
//	public ExToken createToken() {
//		return _msi.createToken();
//	}
	void load密钥() {
		String 公钥 = MyConfig.loadTxt("cert/公钥");
		// System.out.println(公钥);
		if (!MyFun.isEmpty(公钥))
			K公钥 = MyRsa.generate64公钥(公钥);
		String 私钥 = MyConfig.loadTxt("cert/私钥");
		// System.out.println(私钥);
		if (!MyFun.isEmpty(私钥))
			K私钥 = MyRsa.generate64私钥(私钥);
	}

	/**
	 * token 发生器
	 */
	protected transient ExTokenFactory _etlist = null;// 60s过期
	ExBlockMap<String, String> _阻塞map = new ExBlockMap<>(6000);

	public TMs() {
		_etlist = new ExTokenFactory();
	}

////////////////////////extoken ////////////////////
	/**
	 * 只可用一次 在默认过期时间内使用
	 * 
	 * @return
	 */
	public ExToken createExToken() {
		return _etlist.createAdd();
	}

	/**
	 * 只可以用一次 在过期时间内使用
	 * 
	 * @param 过期毫秒
	 * @return
	 */
	public ExToken createExToken(int 过期毫秒) {
		return _etlist.createAdd(过期毫秒);
	}

	/**
	 * 只可以用一次 令牌ID自己自定义 主要用于测试
	 * 
	 * @param 过期毫秒
	 * @return
	 */
	public ExToken createExToken(String 指定令牌) {
		return _etlist.createAdd(指定令牌);
	}

	/**
	 * 仅判断
	 * 
	 * @param token
	 * @return
	 */
	public boolean containExToken(String token) {
		return _etlist.contain(token);
	}

	/**
	 * 判断后 使用标记使用次数加一
	 * 
	 * @param token
	 * @return
	 */
	public boolean contain1ExToken(String token) {
		return _etlist.contain_1(token);
	}

	public ExToken getExToken(String token) {
//		if (MyFun.isEmpty(token)) {
//			return _etlist.createAdd();
//		}
		return _etlist.get(token);
	}

	public void removeExToken(String token) {
		_etlist.remove(token);
	}

	public void showExTokens() {
		_etlist.show();

	}

	public static Netpg2Msgs1 generate反馈(byte[] bytes, String... msgs) {
		Netpg2Msgs1 np = new Netpg2Msgs1();
		np.set(I请求应答, bytes, msgs);
		return np;
	}

/////////////////////////////////dao//////////////////////////////
	public Map<String, Object> getUser(String auser) {
		String sql = "select id,uname from user where uname=? or uphone=?";
		Object[] params = { auser, auser };
		return getDB().queryMap(sql, params);
	}

	public Integer getUserByName(String auser) {
		String sql = "select id from user where  uname=? ";
		Object[] params = { auser };
		return MyFun.obj2Int(getDB().querySingle(sql, params));
	}

	public Map<String, Object> getUserByID(Integer userid) {
		// 最多保留10个备份
		String sql = "select * from user  where id =?";
		Object[] params = { userid };
		return getDB().queryMap(sql, params);
	}

}
