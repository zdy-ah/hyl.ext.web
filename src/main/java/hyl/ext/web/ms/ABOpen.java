package hyl.ext.web.ms;

import java.io.Serializable;
import java.util.Map;

import hyl.core.MyFun;

public class ABOpen implements Serializable {
	/**
	 * session 持续一段时间 而 token 立即失效
	 */
	private static final long serialVersionUID = 1L;
	public String openid; // B系统用户与A系统的映射账号的关联id 唯一的
	public Integer uidA; // A系统用户id 固定的
	public String unameA; // A系统用户id 固定的
	public Integer uidB; // B系统用户id 固定的
	public String unameB; // A系统用户id 固定的
	public String sidA; // B系统会话sessionid 可更新
	public String sidB; // B系统会话sessionid 可更新
	public String ipB; // B系统会话sessionid 可更新
	public String appB; // B系统appB=appid固定的
	public String ok = "y"; // 状态 y 可用 n 禁止登录

	public ABOpen() {

	}
	public String getSUidA() {
		return String.valueOf(uidA);
	}
	public String getSUidB() {
		return String.valueOf(uidB);
	}
	public ABOpen(Map<String, Object> row) {
		openid = MyFun.obj2Str(row.get("openid"));
		uidA = MyFun.obj2Int(row.get("uida"));
		unameA = MyFun.obj2Str(row.get("unamea"));
		uidB = MyFun.obj2Int(row.get("uidb"));
		unameB = MyFun.obj2Str(row.get("unamea"));
		sidA = MyFun.obj2Str(row.get("sida"));
		sidB = MyFun.obj2Str(row.get("sidb"));
		ipB = MyFun.obj2Str(row.get("ipb"));
		appB = MyFun.obj2Str(row.get("appb"));
		ok = MyFun.obj2Str(row.get("ok"));
	}
	public void show() {
		 MyFun.print(openid,uidA,unameA,uidB,unameB,sidA,sidB,ipB,appB,ok);
	}
}
/*
 * a和b 用户的关系 比我原来想象的复杂
 * 
 * 有没有可能 b中的多个用户对应的是 a中的一个用户?
 * 
 * 有没有可能 a中的多个用户对应 b中的一个用户?
 * 
 * 为了避免这种情况, 规定 a与b 的用户必须一一对应
 *
 * b 登录 到a 时自动绑定关系
 * 
 * a 登录 b 是不绑定关系, 因为绑定 需要在 b侧开发绑定代码
 * 
 */