package hyl.ext.ws;

import java.io.File;
import java.util.Date;
import java.util.List;

import hyl.core.MyFun;
import hyl.core.fun.MyDate2;
import hyl.core.io.MyFile;
import hyl.core.run.IDo1;
import hyl.ext.ws.msg.Msgpg;
import hyl.ext.ws.msg.MsgpgStream;
import hyl.ext.ws.msg.WsMsgS;

/**
 * WsRoom 对象
 * 
 * 表示房间
 * 
 * 主要用于存储历史聊天消息
 * 
 * 房间可以根据需求使用 WsMsgS格式 或者 Msgpg格式保存消息
 * 
 * 带有日期的是长久保存
 * 
 * 不带日期的是临时保存
 * 
 * 
 * 
 * 
 * @author 37798955@qq.com
 *
 */
public class WsRoom {
	//房间分类 , 不同类型的房间使用不同的模型
	public final static String D双层="双";
	public final static String D三层="三";
	public final static String D客服="服";
	public String _模型 = D双层; // 默认是2
	static String _会话目录 = "./tmpmsg";
	File _rafile = null;

	/**
	 * 房号或者房主id或者 公司+房主
	 */
	public String RID;

	public WsRoom(String 编号) {
		RID = 编号;
	}

	public WsRoom(String 编号, String 模型) {
		RID = 编号;
		_模型 = 模型;
	}

	public String toString() {
		return RID;
	}

	// 考虑 留言的情况 可以使用的函数
	/**
	 * 追加数据 适用于多种格式
	 * 
	 * @param 数据
	 */
	public synchronized void saveMsg(byte[] 数据) {
		if (_rafile == null)
			_rafile = MyFile.openFile(MyFun.join2("/",_会话目录, "未阅",  _模型,  RID));
		MyFile.appendBytes(_rafile, 数据);
	}

	////////////////////// WsMsgS 格式 临时记录/////////////////
	public synchronized void saveMsgs(WsMsgS 数据) {
		if (_rafile == null)
			_rafile = MyFile.openFile(MyFun.join2("/",_会话目录, "未阅",  _模型, RID));
		MyFile.appendBytes(_rafile, 数据.encode());
	}

	/**
	 * 保存未处理数据
	 * 
	 * @return
	 */
	public synchronized List<WsMsgS> loadMsgs() {
		if (_rafile == null)
			return null;
		byte[] data = MyFile.readAllBytes(_rafile);
		List<WsMsgS> msgs = WsMsgS.loadMsgs(data);
		MyFile.deleteFile(_rafile);
		return msgs;
	}

	/**
	 * 取出未处理信息
	 * 
	 * @return
	 */
	public synchronized List<String> loadMsgsJson() {
		if (_rafile == null)
			return null;
		byte[] data = MyFile.readAllBytes(_rafile);
		List<String> msgs = WsMsgS.loadMsgjson(data);
		MyFile.deleteFile(_rafile);
		return msgs;
	}

	/**
	 * 取出未处理信息
	 * 
	 * @param 读取
	 */
	public synchronized void forMsgs(IDo1<WsMsgS> 读取) {
		if (_rafile == null)
			return;
		byte[] data = MyFile.readAllBytes(_rafile);
		WsMsgS.foreach(data, 读取);
		MyFile.deleteFile(_rafile);
		return;
	}

	////////////////////// Msgpg 格式 临时记录/////////////////
	/**
	 * 保存未读取信息
	 * 
	 * @param 数据
	 */
	public synchronized void saveMsgpg(Msgpg 数据) {
		if (_rafile == null || !_rafile.exists())
			_rafile = MyFile.openFile(MyFun.join2("/",_会话目录, "未阅", _模型, RID));
		MyFile.appendBytes(_rafile, 数据.toSendBytes());
	}

	/**
	 * 读取待处理消息
	 * 
	 * @return
	 */
	public synchronized List<Msgpg> loadMsgpg() {
		if (_rafile == null)
			return null;
		byte[] data = MyFile.readAllBytes(_rafile);
		MsgpgStream wmbs = MsgpgStream.getInstance();
		List<Msgpg> msgs = wmbs.add碎片(data).toList();
		MyFile.deleteFile(_rafile);
		return msgs;
	}

	/**
	 * 取出未处理信息
	 * 
	 * @param 读取
	 */
	public synchronized void forMsgpg(IDo1<Msgpg> 读取) {
		if (_rafile == null)
			return;
		byte[] data = MyFile.readAllBytes(_rafile);
		MsgpgStream wmbs = MsgpgStream.getInstance();
		wmbs.add碎片(data).foreach(读取);
		MyFile.deleteFile(_rafile);
		return;
	}

	//////////////////// 按日期保存 ////
	// 不建议使用 数据存储需要空间 建议使用前端存储引擎
	// 但是在客服环境中 留言的作用要被放大,所以必须使用
	//////////////////// /////////////////

	/////////////////////// WsMsgS 格式 历史记录 //////////////////////
	int 分区数 = 2;

	public void set日分区数(int 分区数) {
		this.分区数 = (分区数 < 2) ? 2 : 分区数;
	}

	int 上次分区 = 0;

	/**
	 * 如果当前日分区与上次分区不一样 就删除当前分区数据 ,并把当前分区赋值给上次分区
	 * 
	 * @param 日期
	 * @return
	 */
	public int get日分区(Date 日期) {
		int 分区 = MyDate2.get日分区(日期, 分区数);
		if (分区 != 上次分区 && (分区 > 上次分区 || 分区 == 0)) {
			上次分区 = 分区;
			del日消息(分区);
		} else {
			// 否则就是补分区数据的情况
		}
		return 分区;
	}

	/**
	 * 保存日聊天记录
	 * 
	 * @param 日期
	 * @param 数据
	 */
	public synchronized void save日消息(Date 日期, byte[] 数据) {
		int 分区 = get日分区(日期);
		File hisfile = MyFile.openFile(MyFun.join2("/", _会话目录, "备", _模型,   RID,  分区));
		MyFile.appendBytes(hisfile, 数据);
	}

	/**
	 * 拉取日聊天记录 处理方式有
	 * 
	 * 1)WsMsgS.loadMsgs(data);
	 * 
	 * 2)MsgpgStream wmbs = MsgpgStream.getInstance();
	 * 
	 * wmbs.add碎片(data).foreach(读取);
	 * 
	 * @param 日期
	 * @return
	 */
	public synchronized byte[] load日消息() {
		// 这里就是上次分区 原因是 取最近分区的消息
		File hisfile = MyFile.openFile(MyFun.join2("/",_会话目录, "备", _模型, RID, 上次分区));
		return MyFile.readAllBytes(hisfile);
	}

	///////////////////// 删除记录////////////////////////
	/**
	 * 删除日聊天记录 适用于多种格式
	 * 
	 * @param 日期
	 */
	synchronized void del日消息(int 分区) {
		File hisfile = MyFile.openFile(MyFun.join2("/",_会话目录,  "备", _模型,  RID, 分区));
		MyFile.deleteFile(hisfile);
	}

}
