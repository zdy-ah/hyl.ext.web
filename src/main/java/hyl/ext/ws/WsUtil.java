package hyl.ext.ws;

import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import javax.websocket.Session;
import javax.websocket.RemoteEndpoint.Async;

public class WsUtil {
	static final String er内容不能为空 = "发送内容不能为空";
	static final String er主题不能为空 = "发送主题不能为空";
	static final String er发送人不能为空 = "发送人不能为空";
	static final String er接收人不能为空 = "接收人不能为空";
	static final String 成功 = "成功";
	static final String 失败 = "失败";
	static final String 无处理 = "无处理";
	/**
	 * 获取远程客户端ip地址
	 * @param session
	 * @return
	 */
	public static InetSocketAddress getRemoteAddress(Session session) {
		if (session == null) {
			return null;
		}
	
		Async async = session.getAsyncRemote();
		// 在Tomcat 8.0.x版本有效
//		InetSocketAddress addr = (InetSocketAddress) getFieldInstance(async,"base#sos#socketWrapper#socket#sc#remoteAddress");
		// 在Tomcat 8.5以上版本有效
		InetSocketAddress addr = (InetSocketAddress) getFieldInstance(async, "base#socketWrapper#socket#sc#remoteAddress");
		return addr;
	}
	private static Object getFieldInstance(Object obj, String fieldPath) {
		String fields[] = fieldPath.split("#");
		for (String field : fields) {
			obj = getField(obj, obj.getClass(), field);
			if (obj == null) {
				return null;
			}
		}
		return obj;
	}
	private static Object getField(Object obj, Class<?> clazz, String fieldName) {
		for (; clazz != Object.class; clazz = clazz.getSuperclass()) {
			try {
				Field field;
				field = clazz.getDeclaredField(fieldName);
				field.setAccessible(true);
				return field.get(obj);
			} catch (Exception e) {
			}
		}
		return null;
	}
	


}
