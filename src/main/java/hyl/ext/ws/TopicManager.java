package hyl.ext.ws;

import java.util.Map;
import java.util.Set;
import hyl.base.cache.redis.CRedis;
import hyl.base.cache.redis.RdCacheSs;
import hyl.core.Amy;
import hyl.core.MyFun;

/**
 * 订阅人可以是一个组 也可以是一个人
 * @author 37798955@qq.com
 *
 */
public class TopicManager {

	String RD别名 = "redis1";
	/**
	 * 一个主题 对应多个客户
	 */
	protected RdCacheSs _主题集 = null;


	/**
	 * 一个客户对应多个主题
	 */
	//protected RdCacheSs _客户集 = null;

	public TopicManager() {
		init(Amy.RD1, null);
	}

	public TopicManager(String Redis别名, Integer 主题库) {
		init(Redis别名, 主题库);
	}

	void init(String Redis别名, Integer 主题库) {
	
		RD别名 = MyFun.nvlStr(Redis别名, Amy.RD1);
		_主题集 = new RdCacheSs(RD别名, MyFun.nvlInt(主题库, CRedis.IDX_TOPIC));
	//	_客户集 = new RdCacheSs(RD别名, MyFun.nvlInt(客户库, CRedis.IDX_USERTOPIC));

	}

	public Map<String, Set<String>> get所有主题的订阅人() {
		return _主题集.getLike("*");
	}

//	public Map<String, Set<String>> get客户相关() {
//		return _客户集.getLike("*");
//	}

	public Map<String, Set<String>> get相似主题的订阅人(String like) {
		return _主题集.getLike(like);
	}

//	public Map<String, Set<String>> get客户相关(String like) {
//		return _客户集.getLike(like);
//	}
/**
 * 查询 订阅主题 的人有哪些
 * @param 主题
 * @return
 */
	public Set<String> get某主题的订阅人(String 主题) {
		return _主题集.get(主题);
	}

//	public Set<String> get客户的订阅(String 用户) {
//		return _客户集.get(用户);
//	}
/**
 * 查询相似的主题
 * @param 主题
 * @return
 */
	public Set<String> get相似主题(String 主题) {
		return _主题集.keys(主题);
	}

//	public Set<String> get客户(String 客户) {
//		return _客户集.keys(客户);
//	}

	/** 一个客户端可以订阅多个主题 */
	public void f订阅主题(String 订阅人, String 主题) {
		if (MyFun.isEmpty(主题) && MyFun.isEmpty(订阅人))
			return;
		_主题集.put(主题, 订阅人);
		//_客户集.put(订阅人, 主题);
	}

	/**
	 * 注销订阅
	 * 
	 * @param topic
	 */
	public void f取消订阅(String 订阅人, String 主题) {
		if (MyFun.isEmpty(主题) && MyFun.isEmpty(订阅人))
			return;
		_主题集.remove(主题, 订阅人);
		//_客户集.remove(订阅人, 主题);
	}

	/**
	 * 一个主题被多个客户订阅
	 * 
	 * @param 客户集
	 * @param 主题
	 */
	public void f批量订阅(String 主题, String... 订阅人集) {
		for (String 订阅人 : 订阅人集) {
			f订阅主题(订阅人, 主题);
		}
	}

	/**
	 * 一个客户订阅多个主题
	 * 
	 * @param 客户
	 * @param 主题集
	 */
	public void f批量订阅主题(String 订阅人, String... 主题集) {
		for (String 主题 : 主题集) {
			f订阅主题(订阅人, 主题);
		}
	}

	public void f批量退订(String 主题, String... 订阅人集) {
		for (String 订阅人 : 订阅人集) {
			f取消订阅(订阅人, 主题);
		}
	}

	public void f批量退订主题(String 订阅人, String... 主题集) {
		for (String 主题 : 主题集) {
			f订阅主题(订阅人, 主题);
		}

	}

	public void f删除主题(String 主题) {
		Set<String> 用户集 = _主题集.get(主题);
		if (用户集 != null) {
//			for (String 用户 : 用户集) {
//				_客户集.remove(用户, 主题);
//			}
			_主题集.remove(主题);
		}
	}

//	public void f移除客户(String 订阅人) {
//		Set<String> 主题集 = _客户集.get(订阅人);
//		if (主题集 != null) {
//			for (String 主题 : 主题集) {
//				_主题集.remove(主题, 订阅人);
//			}
//			_客户集.remove(订阅人);
//		}
//
//	}
}
