package hyl.ext.ws;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * WsRoomMen 对象 继承 WsRoom 对象
 * 
 * 主要扩展定义了房间成员和房间密钥
 * 
 * 可以理解为 一个群组对象有多个成员 或者理解为一个用户对象 有多个相关在线终端
 * 
 * 每个用户或终端登录成功以后分配一个独立的密钥
 * 
 * @author 37798955@qq.com
 *
 */
public class WsRoomMen extends WsRoom{

	protected String AES密钥;
	/**
	 * 房间里的在线终端 
	 */
	public Set<WsServe> _在线终端 = null;
	public WsRoomMen(String 组编号,String 模型) {
		super(组编号);
		_模型=模型;
	}

	public WsRoomMen(String 组编号) {
		super(组编号);
		_模型=WsRoom.D三层;
	}
	// 注意锁 此处有bug
	public synchronized void add(WsServe 终端) {
		if (_在线终端 == null)
			_在线终端 = new HashSet<>();
		_在线终端.add(终端);
	}

	public synchronized void remove(WsServe 终端) {
		_在线终端.remove(终端);
		if (_在线终端.isEmpty())
			_在线终端 = null;
	}

	public void create密钥() {
//		MyDes mDes=	MyDes.getInstance();
//		mDes.createKey(密码)

	}

	public String get密钥() {
		return AES密钥;
	}

	
}
