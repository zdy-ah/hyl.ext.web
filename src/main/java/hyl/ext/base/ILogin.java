package hyl.ext.base;


import java.util.Map;

/**
 * 规定了用户数据库处理的基础方法
 * 
 * @author 37798955@qq.com
 *
 */
public interface ILogin {
	/*
	 * 检查用户 登录账号和密码 并完成会话填充
	 * */

	//Content checkLogin(MySession 会话, String 账号, String 密码);
	/**
	 * 把用户属性 保存到会话中 
	 * 
	 * 每个系统用户表可能会不一样
	 * @param tk 会话
	 * @param user 用户属性
	 */
	void setLoginSession(MySession tk, Map<String, Object> user);
}
