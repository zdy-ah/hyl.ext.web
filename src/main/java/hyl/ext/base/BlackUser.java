package hyl.ext.base;
import java.util.HashSet;
public class BlackUser {
	private static BlackUser bu;
	public static BlackUser getInstance() {
		if (bu == null)
			bu = new BlackUser();
		return bu;
	}
	private HashSet<String> hs = new HashSet<>();
	public boolean containIP(String ip) {
		return hs.contains(ip);
	}
	public void addIP(String ip) {
		hs.add(ip);
	}
	public void removeIP(String ip) {
		hs.remove(ip);
	}
}
