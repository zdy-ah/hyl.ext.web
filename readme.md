
# 序言

我们需要 对session 缓存 进行管理

最终要实现 session 集群可用

这是基于servlet 的组件

# 需求范围


Wsbasecontroller 是websocket 工具基类
设计目的
1 发送消息给具体某人,对方无须订阅
2 对方订阅某个主题,发送消息给主题,对方可以接受到消息
3 2021-4-24 希望实现系统间发送消息
4 服务端可以为客户设定主题 但是客户端订阅主题需要服务端授权
5 消息通用格式 {cmd:"",}


设计的边界
用户没有订阅的情况下,发送主题类消息,用户无法接收
解决方法,连接后, 历史消息在handler实现时,预先加载. 然后在接收发送消息

继承 Wsbasecontroller 后 需要重写 hangler方法 完成数据接收后的处理逻辑

如果想把ws 服务实现不同系统间数据传输,





# bug
20201026 
	1) MySession getSessionById(MySession session,String ip)
	ip 判断不能带上端口,ajax 和 webview 会使用不同的端口 只要判断ip即可
	如果一个session的 ip 变更了,才需要重新新建session 否则不需要

# 日志


计划 VR002 
	改进WsBaseController 用队列发送(实现消峰),小系统不存在这种问题

2021-4-24 
	
	
2020-1-1 VR001 基本实现收发功能